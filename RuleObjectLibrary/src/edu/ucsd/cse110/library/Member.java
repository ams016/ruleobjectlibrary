package edu.ucsd.cse110.library;

public class Member {
	private MemberType memberType;
	private String name;
	public double fees;
    private LateFeeApplier lfa;

	public Member(String string, MemberType type) {
		setName(string);
        lfa = new LateFeeApplier();
		memberType = type;
	}

	public double getDueFees() {
		return lfa.getDueFees(this);
	}

	public void applyLateFee(double i) {
		lfa.applyLateFee(this,i);
	}
	
	public MemberType getType() {
		return memberType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    public void notifyTomorrow(Publication p)
    {
        System.out.println("You have a publication due tomorrow.");
    }

}
