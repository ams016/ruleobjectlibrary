package edu.ucsd.cse110.library;

public class LateFeeApplier {
	private Member member;

	public LateFeeApplier() {
	}

	public double getDueFees(Member mem) {
		return mem.fees;
	}

	public void applyLateFee(Member mem, double i) {
		mem.fees+=i;
	}


}
