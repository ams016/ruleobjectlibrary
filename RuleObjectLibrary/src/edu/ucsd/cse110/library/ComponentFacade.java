package edu.ucsd.cse110.library;

import java.time.LocalDate;
import java.util.List;

public class ComponentFacade {
	private LateFeeApplier lfa;
    private List<Publication> pubs;

	public ComponentFacade() {
        lfa = new LateFeeApplier();
	}

    public void checkoutPublication(Member mem, Publication pub)
    {
        pub.checkout(mem, LocalDate.now());

    }

    public void returnPublication(Publication pub)
    {
        pub.pubReturn( LocalDate.now() );
    }

    public double getFee(Member mem)
    {
        return lfa.getDueFees(mem);
    }

    public boolean hasFee(Member mem)
    {
        if (lfa.getDueFees(mem) > 0)
            return true;

        return false;
    }
}
