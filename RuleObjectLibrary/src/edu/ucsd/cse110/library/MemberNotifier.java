package edu.ucsd.cse110.library;

import net.sf.cglib.core.Local;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class MemberNotifier {
    List<Publication> pubs;
    public MemberNotifier()
    {

    }

    public void checkForBooksDueTomorrow() {
        for (Publication p : pubs) {
            if (!p.isCheckout()) {
                continue;
            }

            if (LocalDate.now().compareTo(p.getCheckoutDate()) <= 1) {
                p.getMember().notifyTomorrow(p);
            }
        }
    }
}
